package com.sugarreport.web;

import ch.qos.logback.classic.Logger;
import com.mongodb.DBObject;
import com.mongodb.util.JSON;
import org.slf4j.LoggerFactory;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletResponse;

@Controller
@RequestMapping("/support")
public class SupportController {

    private static Logger errorLogger = (Logger) LoggerFactory.getLogger("ERROR-LOGGER");
    private static Logger notificationLogger = (Logger) LoggerFactory.getLogger("NOTIFICATION-LOGGER");
    private static Marker marker = MarkerFactory.getMarker("SEND_MAIL");

    @RequestMapping(method = RequestMethod.POST)
    public void support(@RequestBody String messageJson, HttpServletResponse response) {
        try {
            DBObject message = (DBObject) JSON.parse(messageJson);
            notificationLogger.info(marker, "Message from : {} text: {}", message.get("email"), message.get("text"));
            response.setStatus(HttpServletResponse.SC_OK);
        } catch (Exception ex) {
            errorLogger.error("SupportController error message: " + messageJson, ex);
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        }
    }
}

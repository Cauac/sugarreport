package com.sugarreport.web;

import ch.qos.logback.classic.Logger;
import com.sugarreport.dao.UserDAO;
import com.sugarreport.service.mail.MailService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletResponse;

@Controller
@RequestMapping("restorePassword")
public class RestorePasswordController {

    private static Logger logger = (Logger) LoggerFactory.getLogger("ERROR-LOGGER");

    @Autowired
    UserDAO userDAO;

    @Autowired
    MailService mailService;

    @Value("#{mail.restore_password_message_subject}")
    private String messageSubject;

    @Value("#{mail.restore_password_massage_text}")
    private String messageText;

    @RequestMapping(method = RequestMethod.POST)
    public void restorePassword(@RequestBody String email, HttpServletResponse httpServletResponse) {
        try {
            email = StringUtils.trim(email);
            if (StringUtils.isEmpty(email)) {
                httpServletResponse.setStatus(HttpServletResponse.SC_CONFLICT);
                return;
            }
            String password = userDAO.getUserPassword(email);
            if (password == null) {
                httpServletResponse.setStatus(HttpServletResponse.SC_CONFLICT);
                return;
            }
            mailService.sendRegisterMessage(email, messageSubject, String.format(messageText, password));
            httpServletResponse.setStatus(HttpServletResponse.SC_OK);
        } catch (Exception ex) {
            logger.error("Restore password error mail:" + email, ex);
            httpServletResponse.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
    }
}

package com.sugarreport.dao;

import com.mongodb.*;
import org.springframework.beans.factory.annotation.Required;

public abstract class MongoDAO {

    protected static final String IDENTITY = "_id";
    protected static final DBObject WITH_IDENTITY = new BasicDBObject(IDENTITY, 1);

    private String collectionName;
    protected DB database;
    protected DBCollection collection;

    @Required
    public void setDatabase(DB db) {
        this.database = db;
        this.collection = db.getCollection(collectionName);
    }

    @Required
    public void setCollectionName(String collectionName) {
        this.collectionName = collectionName;
    }
}

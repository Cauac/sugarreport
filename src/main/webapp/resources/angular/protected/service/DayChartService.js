(function () {
    'use strict';
    angular.module('SugarReportProtected').directive("chart", function (DayChartService) {
        return {
            restrict: "A",
            scope: {},
            template: "<div id=\"chartdiv\" style=\"width: 100%; height: 400px;\">",
            link: function () {
                DayChartService.readyToRender();
            }
        };
    });
    angular.module('SugarReportProtected').service('DayChartService', function ($http, $q, $modal, DayDataService) {

        var chart;
        var chartData = [];
        var readyToRender = false;

        this.readyToRender = function () {
            readyToRender = true;
            if (chartData.length > 2) {
                createChart();
            }
        };

        this.drawChart = function () {
            chartData = [];
            chartData.push({time: '00:00'});
            chartData = chartData.concat(DayDataService.getMergedSugarDays());
            chartData.push({time: '23:59'});
            if (chartData.length == 2) {
                readyToRender = false;
            } else if (readyToRender) {
                chart.dataProvider = chartData;
                chart.validateData();
            }
        };

        var createChart = function () {
            chart = AmCharts.makeChart("chartdiv",
                {
                    "type": "serial",
                    "pathToImages": "/resources/amcharts/images/",
                    "balloonDateFormat": "HH:NN",
                    "categoryField": "time",
                    "dataDateFormat": "HH:NN",
                    "autoMarginOffset": 40,
                    "marginRight": 60,
                    "marginTop": 60,
                    "startDuration": 1,
                    "fontSize": 13,
                    "language": "ru",
                    "theme": "light",
                    "categoryAxis": {
                        "centerLabelOnFullPeriod": false,
                        "gridPosition": "start",
                        "markPeriodChange": false,
                        "minPeriod": "5mm",
                        "parseDates": true
                    },
                    "trendLines": [],
                    "legend": {
                        "position": "absolute"
                    },
                    "graphs": [
                        {
                            "balloonText": " [[category]] - [[value]] ммоль/л",
                            "bullet": "round",
                            "bulletSize": 15,
                            "id": "previousDay",
                            "lineAlpha": 1,
                            "lineThickness": 3,
                            "title": "Вчера",
                            "type": "smoothedLine",
                            "valueField": "value2",
                            "showHandOnHover": true
                        },
                        {
                            "balloonText": " [[category]] - [[value]] ммоль/л",
                            "bullet": "square",
                            "bulletSize": 10,
                            "id": "currentDay",
                            "lineAlpha": 1,
                            "lineThickness": 3,
                            "title": "Сегодня",
                            "type": "smoothedLine",
                            "valueField": "value",
                            "showHandOnHover": true
                        }
                    ],
                    "guides": [],
                    "valueAxes": [
                        {
                            "id": "ValueAxis-1",
                            "minimum": 0,
                            "precision": 1,
                            "showFirstLabel": false,
                            "title": ""
                        }
                    ],
                    "allLabels": [],
                    "balloon": {},
                    "titles": [
                        {
                            "id": "Title-1",
                            "size": 15,
                            "text": "Уровень сахара за день"
                        }
                    ],
                    "dataProvider": chartData
                }
            );
            chart.addListener("clickGraphItem", function (args) {
                var item = chartData[args.index];
                $modal.open({
                    templateUrl: '/pages/protected/dialog/sugarDetails.html',
                    controller: SugarDetailsDialog,
                    resolve: {
                        record: function () {
                            return DayDataService.getSugarWithRelatedRecords(item, args.graph.id);
                        }
                    }
                });
            });
        }
    });

    var SugarDetailsDialog = function ($scope, $modalInstance, record) {

        $scope.record = record;
        var date = new Date();
        date.setHours(0, 0, 0, 0);
        var fictiveDate = '1970-01-01T';
        if (!angular.isUndefined($scope.record.food)) {
            $scope.record.food.date = date.getTime() + Date.parse(fictiveDate + $scope.record.time) - Date.parse(fictiveDate + $scope.record.food.time);
        }
        if (!angular.isUndefined($scope.record.injection)) {
            $scope.record.injection.date = date.getTime() + Date.parse(fictiveDate + $scope.record.time) - Date.parse(fictiveDate + $scope.record.injection.time);
        }

        $scope.ok = function () {
            $modalInstance.dismiss('cancel')
        };
    };
})();
package com.sugarreport.service.auth;

import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

public class SecurityService {

    public static final String ROLE_USER = "ROLE_USER";
    public static final SimpleGrantedAuthority ROLE_USER_AUTHORITY = new SimpleGrantedAuthority(ROLE_USER);

    public static boolean isAuthenticated() {
        return SecurityContextHolder.getContext().getAuthentication() != null &&
                SecurityContextHolder.getContext().getAuthentication().getAuthorities().contains(ROLE_USER_AUTHORITY);
    }

    public static String currentUserId(){
        return SecurityContextHolder.getContext().getAuthentication().getName();
    }
}

package com.sugarreport.web;

import com.mongodb.DBObject;
import com.sugarreport.dao.SugarDAO;
import com.sugarreport.dao.UserDAO;
import com.sugarreport.service.report.FileReportService;
import com.sugarreport.service.report.SugarReport;
import com.sugarreport.service.auth.SecurityService;
import org.apache.commons.lang.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.*;

@Controller
@RequestMapping("/report")
public class ReportController {

    @Autowired
    private SugarDAO sugarDAO;
    @Autowired
    private UserDAO userDAO;
    @Autowired
    private FileReportService reportService;


    @RequestMapping(value = "getData", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public
    @ResponseBody
    DBObject getReportData(@RequestBody String datesStr, HttpServletResponse response) throws IOException {
        String[] dates = datesStr.split(";");
        DBObject userSettings = userDAO.getUserSettings(SecurityService.currentUserId());
        double normMin = NumberUtils.toDouble(userSettings.get(UserDAO.Fields.SettingFields.SUGAR_NORM_MIN).toString());
        double normMax = NumberUtils.toDouble(userSettings.get(UserDAO.Fields.SettingFields.SUGAR_NORM_MAX).toString());
        SugarReport report = new SugarReport(normMin, normMax);
        DBObject data = sugarDAO.getSugarValues(SecurityService.currentUserId(), dates);
        DBObject reportResult = report.reduceData(data);
        response.setStatus(HttpServletResponse.SC_OK);
        return reportResult;
    }

    @RequestMapping(value = "getFile", method = RequestMethod.POST)
    public void postFile(String startDate, String endDate, String type, HttpServletResponse response) throws IOException {
        Collection<String> dates = new ArrayList();
        LocalDate sDate = LocalDate.parse(startDate);
        LocalDate eDate = LocalDate.parse(endDate);
        do {
            dates.add(sDate.format(DateTimeFormatter.ISO_DATE));
            sDate = sDate.plus(1, ChronoUnit.DAYS);
        } while (!sDate.isAfter(eDate));
        DBObject data = sugarDAO.getSugarValues(SecurityService.currentUserId(), dates.toArray(new String[dates.size()]));

        DBObject userSettings = userDAO.getUserSettings(SecurityService.currentUserId());
        double normMin = NumberUtils.toDouble(userSettings.get(UserDAO.Fields.SettingFields.SUGAR_NORM_MIN).toString());
        double normMax = NumberUtils.toDouble(userSettings.get(UserDAO.Fields.SettingFields.SUGAR_NORM_MAX).toString());

        SugarReport report = new SugarReport(normMin, normMax, startDate, endDate);
        report.reduceData(data);
        try {
            switch (type) {
                case "pdf":
                    reportService.generateSugarReportPDF(report, response);
                    break;
                case "docx":
                    reportService.generateSugarReportDOCX(report, response);
                    break;
                case "xlsx":
                    reportService.generateSugarReportXLSX(report, response);
                    break;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

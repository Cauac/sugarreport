package com.sugarreport.service.mail;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Service;

@Service
public class MailService {

    @Autowired
    private JavaMailSenderImpl registrationSender;

    public void sendRegisterMessage(String to, String subject, String msg) throws MailException {
        sendMessage(to, subject, msg, registrationSender);
    }

    private void sendMessage(String to, String subject, String msg, JavaMailSenderImpl sender) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(sender.getUsername());
        message.setTo(to);
        message.setSubject(subject);
        message.setText(msg);
        sender.send(message);
    }
}

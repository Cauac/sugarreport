package com.sugarreport.service.auth;

import com.sugarreport.dao.UserDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.ArrayList;

public class DBUserDetailsService implements UserDetailsService {

    @Autowired
    private UserDAO userDAO;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        String password = userDAO.getUserPassword(username);
        if (password == null) {
            throw new UsernameNotFoundException("Username " + username + " not found!");
        }

        ArrayList userAuthorities = new ArrayList();
        userAuthorities.add(SecurityService.ROLE_USER_AUTHORITY);

        return new User(username, password, true, true, true, true, userAuthorities);
    }
}

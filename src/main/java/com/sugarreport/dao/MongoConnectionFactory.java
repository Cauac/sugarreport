package com.sugarreport.dao;

import com.mongodb.DB;
import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.net.UnknownHostException;
import java.util.Arrays;

@Component(value = "connectionFactory")
public class MongoConnectionFactory {

    @Value("#{db.username}")
    private String dbUsername;

    @Value("#{db.password}")
    private String dbPassword;

    @Value("#{db.host}")
    private String dbHost;

    @Value("#{db.name}")
    private String dbName;

    @Value("#{db.port}")
    private int dbPort;

    public DB getConnection() throws UnknownHostException {
        return (new MongoClient(new ServerAddress(dbHost, dbPort), Arrays.asList(MongoCredential.createMongoCRCredential(dbUsername, dbName, dbPassword.toCharArray())))).getDB(dbName);
    }
}

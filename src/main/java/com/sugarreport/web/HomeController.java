package com.sugarreport.web;

import com.mongodb.DBObject;
import com.mongodb.util.JSON;
import com.sugarreport.dao.SugarDAO;
import com.sugarreport.service.auth.SecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;

@Controller
@RequestMapping("/home")
public class HomeController {

    @Autowired
    SugarDAO sugarDAO;

    @RequestMapping(method = RequestMethod.GET)
    public String index() {
        return "/pages/protected/home.html";
    }

    @RequestMapping(value = "addRecord", method = RequestMethod.POST)
    public void addRecord(@RequestBody String jsonData, HttpServletResponse response) {
        DBObject sugar = (DBObject) JSON.parse(jsonData);
        sugarDAO.addRecord(SecurityService.currentUserId(), sugar);
        response.setStatus(HttpServletResponse.SC_NO_CONTENT);
        response.setHeader("Content-Type", "text/plain; charset=utf-8");
    }

    @RequestMapping(value = "removeRecord", method = RequestMethod.POST)
    public void removeRecord(@RequestBody String jsonData, HttpServletResponse response) {
        DBObject record = (DBObject) JSON.parse(jsonData);
        sugarDAO.remove(SecurityService.currentUserId(), record);
        response.setHeader("Content-Type", "text/plain; charset=utf-8");
        response.setStatus(HttpServletResponse.SC_NO_CONTENT);
    }

    @RequestMapping(value = "updateRecord", method = RequestMethod.POST)
    public void updateRecord(@RequestBody String jsonData, HttpServletResponse response) {
        DBObject record = (DBObject) JSON.parse(jsonData);
        sugarDAO.update(SecurityService.currentUserId(), record);
        response.setStatus(HttpServletResponse.SC_NO_CONTENT);
    }

    @RequestMapping(value = "getDayData", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public
    @ResponseBody
    DBObject getDayData(@RequestBody String date, HttpServletResponse response) {
        response.setStatus(HttpServletResponse.SC_OK);
        return sugarDAO.getValuesByDay(SecurityService.currentUserId(), date);
    }

    @RequestMapping(value = "get2DaysData", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public
    @ResponseBody
    DBObject get2DaysData(@RequestBody String dates, HttpServletResponse response) {
        String[] splitDates = dates.split(";");
        response.setStatus(HttpServletResponse.SC_OK);
        return sugarDAO.getValuesByDay(SecurityService.currentUserId(), splitDates);
    }
}

package com.sugarreport.validation;

import com.mongodb.DBObject;
import com.sugarreport.dao.UserDAO;

public class UserSettingsValidator {

    public static boolean isValid(DBObject settings) {
        if (settings == null) {
            return false;
        }
        if (!settings.containsField(UserDAO.Fields.SettingFields.COMPARE_DAYS)) {
            return false;
        }
        if (!settings.containsField(UserDAO.Fields.SettingFields.SUGAR_NORM_MIN)) {
            return false;
        }
        if (!settings.containsField(UserDAO.Fields.SettingFields.SUGAR_NORM_MAX)) {
            return false;
        }
        return true;
    }
}

package com.sugarreport.dao;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

public class SugarDAO extends MongoDAO {

    public interface Fields {
        public static final String DATE = "date";
        public static final String TYPE = "type";
        public static final String TIME = "time";
        public static final String VALUE = "value";
    }

    private static final String RECORD_DAY_IDENTITY_FORMAT = "%s.%s";

    public void addRecord(String userId, DBObject record) {
        String dayId = String.format(RECORD_DAY_IDENTITY_FORMAT, record.removeField(Fields.DATE), record.removeField(Fields.TYPE));
        collection.update(new BasicDBObject(IDENTITY, userId), new BasicDBObject("$push", new BasicDBObject(dayId, record)), true, false);
    }

    public void remove(String userId, DBObject record) {
        String dayId = String.format(RECORD_DAY_IDENTITY_FORMAT, record.removeField(Fields.DATE), record.removeField(Fields.TYPE));
        collection.update(new BasicDBObject(IDENTITY, userId), new BasicDBObject("$pull", new BasicDBObject(dayId, record)), true, false);
    }

    public void update(String userId, DBObject record) {
        String dayId = String.format(RECORD_DAY_IDENTITY_FORMAT, record.removeField(Fields.DATE), record.removeField(Fields.TYPE));
        DBObject oldObject = new BasicDBObject(Fields.TIME, record.get(Fields.TIME));
        collection.update(new BasicDBObject(IDENTITY, userId), new BasicDBObject("$pull", new BasicDBObject(dayId, oldObject)), true, false);
        collection.update(new BasicDBObject(IDENTITY, userId), new BasicDBObject("$push", new BasicDBObject(dayId, record)), true, false);
    }

    public DBObject getValuesByDay(String userId, String date) {
        DBObject fieldObject = new BasicDBObject(IDENTITY, 0);
        fieldObject.put(date, 1);
        DBObject searchResult = collection.findOne(new BasicDBObject(IDENTITY, userId), fieldObject);
        return searchResult == null ? new BasicDBObject() : searchResult;
    }

    public DBObject getValuesByDay(String userId, String[] dates) {
        DBObject fieldObject = new BasicDBObject(IDENTITY, 0);
        for (String date : dates) {
            fieldObject.put(date, 1);
        }
        DBObject searchResult = collection.findOne(new BasicDBObject(IDENTITY, userId), fieldObject);
        return searchResult == null ? new BasicDBObject() : searchResult;
    }

    public DBObject getSugarValues(String userId, String[] dates) {
        DBObject fieldObject = new BasicDBObject(IDENTITY, 0);
        for (String date : dates) {
            fieldObject.put(date + ".sugar", 1);
        }
        DBObject searchResult = collection.findOne(new BasicDBObject(IDENTITY, userId), fieldObject);
        return searchResult == null ? new BasicDBObject() : searchResult;
    }
}

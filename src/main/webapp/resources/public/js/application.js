(function () {
    'use strict';

    var SugarReportPublic = {};

    var App = angular.module('SugarReportPublic', ['ngRoute', 'ui.bootstrap.alert']);

    App.config(['$routeProvider', function ($routeProvider) {

        $routeProvider.when('/hello', {
            templateUrl: 'pages/public/hello.html',
            controller: IndexController
        });
        $routeProvider.when('/signIn', {
            templateUrl: 'pages/public/signIn.html',
            controller: SignInController
        });
        $routeProvider.when('/signUp', {
            templateUrl: 'pages/public/signUp.html',
            controller: SignUpController
        });

        $routeProvider.when('/restorePassword', {
            templateUrl: 'pages/public/restorePassword.html',
            controller: RestorePasswordController
        });

        $routeProvider.when('/about', {
            templateUrl: 'pages/public/about.html',
            controller: IndexController
        });

        $routeProvider.when('/500', {
            templateUrl: 'pages/public/500.html',
            controller: IndexController
        });

        $routeProvider.when('/404', {
            templateUrl: 'pages/public/404.html',
            controller: IndexController
        });

        $routeProvider.otherwise({redirectTo: '/hello'});

    }]);

    App.controller('MenuController', function ($scope, $http) {
        $scope.loggedIn = false;
        $scope.showMenu = false;
        $scope.isLoggedIn = function () {
            $http.get('/isAuthenticated').success(function (user) {
                $scope.loggedIn = "anonymousUser" != user;
            });
            $scope.loggedIn = false;
        };
        $scope.isLoggedIn();
    });

    var IndexController = function ($scope) {};
    function RestorePasswordController($scope, $http, $location) {
        $scope.email = '';
        $scope.formVisible = true;
        $scope.alerts = [];
        $scope.closeAlert = function (index) {
            $scope.alerts.splice(index, 1);
        };
        $scope.restore = function () {
            $http.post("/restorePassword", $scope.email).success(function () {
                $scope.formVisible = false;
            }).error(function (data, status) {
                if (status == 409) {
                    $scope.alerts.push({type: 'danger', msg: 'Пользователя с таким e-mail не существует.'});
                } else {
                    $location.url('/500');
                }
            });
        };
    }


    function SignInController($scope, $location, $window, AuthenticationService) {

        var messages = {
            failed: {type: 'danger', msg: 'Введены неправильные email или пароль.'},
            successRegister: {type: 'success',msg: 'Аккаунт активирован. Воспользуйтесь вашими реквизитами для входа на сайт.'}
        };

        $scope.alerts = [];
        $scope.closeAlert = function (index) {
            $scope.alerts.splice(index, 1);
        };

        var params = $location.search();
        if (params.successRegister) {
            $scope.alerts.push(messages['successRegister']);
        }
        if (params.failed) {
            $scope.alerts.push(messages['failed']);
        }

        $scope.login = function () {
            var formData = {'username': $scope.username, 'password': $scope.password, 'remember-me': "on"};
            if ($scope.rememberMe) {
                formData['remember-me'] = "on";
            }
            AuthenticationService.login(formData).then(function () {
                $window.location.href = '/home';
            }, function () {
                $scope.alerts.push(messages['failed']);
            });
        }
    }

    function SignUpController($scope, $location, $http) {

        $scope.user = {};
        $scope.formVisible = true;

        var messages = {
            lost: {type: 'danger', msg: 'Неудача! Время для активации вашего аккаунта истекло. Попробуйте ещё раз.'},
            exist: {type: 'warning', msg: 'Извините, но этот e-mail адрес уже закреплен за другим пользователем!'},
            failed: {type: 'danger', msg: 'Регистрация невозможна. Пожалуйста, проверте ваш e-mail адрес!'}
        };

        $scope.alerts = [];
        $scope.closeAlert = function (index) {
            $scope.alerts.splice(index, 1);
        };

        var params = $location.search();
        if (params.lost) {
            $scope.alerts.push(messages['lost']);
        }

        $scope.register = function () {
            $http.post("/register", $scope.user).success(function () {
                $scope.formVisible = false;
            }).error(function (data, status) {
                if (status == 409) {
                    $scope.alerts.push(messages['exist']);
                }
                if (status == 400) {
                    $scope.alerts.push(messages['failed']);
                }
            });
        };
    }

    function SupportController($scope, $http, $location) {
        $scope.formVisible = true;
        $scope.send = function () {
            $http.post("/support/", $scope.message)
                .success(function () {
                    $scope.formVisible = false;
                })
                .error(function () {
                    $location.url('/500');
                });
        };
    };

    App.service("AuthenticationService", function ($http, $q) {

        this.login = function (formData) {
            var d = $q.defer();
            $http({
                method: 'POST',
                url: "/j_spring_security_check",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                transformRequest: function (obj) {
                    var str = [];
                    for (var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                data: formData
            }).success(function () {
                d.resolve();
            }).error(function () {
                d.reject();
            });

            return d.promise;
        };
    });
})();



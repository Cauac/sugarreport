package com.sugarreport.service.report;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.sugarreport.dao.SugarDAO;
import net.sf.jasperreports.engine.JRDataSource;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

public class SugarReport {

    public interface ReportFields {
        public static final String DATE = "date";
        public static final String COUNT = "count";
        public static final String MINIMUM = "min";
        public static final String MAXIMUM = "max";
        public static final String AVERAGE = "avg";
        public static final String NORMAL_RECORD_COUNT = "normCount";
        public static final String LESS_NORMA_RECORD_COUNT = "lessNormCount";
        public static final String ABOVE_NORMA_RECORD_COUNT = "aboveNormCount";
    }

    protected LocalDate startDate;
    protected LocalDate endDate;
    protected double normMin;
    protected double normMax;
    protected BasicDBList dates = new BasicDBList();
    protected int totalCount = 0;
    protected int totalNormCount = 0;
    protected int totalLessNormCount = 0;
    protected int totalAboveNormCount = 0;
    protected double totalMin = Double.MAX_VALUE;
    protected double totalMax = Double.MIN_VALUE;
    protected double totalAvg = 0.0d;

    public SugarReport(double normMin, double normMax) {
        this.normMin = normMin;
        this.normMax = normMax;
    }

    public SugarReport(double normMin, double normMax, String startDate, String endDate) {
        this.normMin = normMin;
        this.normMax = normMax;
        this.startDate = LocalDate.parse(startDate);
        this.endDate = LocalDate.parse(endDate);
    }

    public DBObject reduceData(DBObject data) {
        DBObject reducedData = new BasicDBObject();
        for (String date : data.keySet()) {
            DBObject day = (DBObject) data.get(date);
            BasicDBList sugars = (BasicDBList) day.get("sugar");
            if (sugars.size() > 0) {
                reduceDay(date, sugars);
            }
        }
        if (dates.size() > 0) {
            totalAvg = (double) Math.round((totalAvg / dates.size()) * 100.0) / 100.0;
        }
        reducedData.put("dates", dates);
        reducedData.put(ReportFields.COUNT, totalCount);
        reducedData.put(ReportFields.MINIMUM, totalMin);
        reducedData.put(ReportFields.MAXIMUM, totalMax);
        reducedData.put(ReportFields.AVERAGE, totalAvg);
        reducedData.put(ReportFields.NORMAL_RECORD_COUNT, totalNormCount);
        reducedData.put(ReportFields.LESS_NORMA_RECORD_COUNT, totalLessNormCount);
        reducedData.put(ReportFields.ABOVE_NORMA_RECORD_COUNT, totalAboveNormCount);
        return reducedData;
    }

    public Map getParams() {
        Map params = new HashMap();
        params.put(ReportFields.COUNT, totalCount);
        params.put(ReportFields.MAXIMUM, totalMax);
        params.put(ReportFields.MINIMUM, totalMin);
        params.put(ReportFields.AVERAGE, totalAvg);
        params.put(ReportFields.NORMAL_RECORD_COUNT, totalNormCount);
        params.put(ReportFields.LESS_NORMA_RECORD_COUNT, totalLessNormCount);
        params.put(ReportFields.ABOVE_NORMA_RECORD_COUNT, totalAboveNormCount);
        params.put("START_DATE", startDate.format(DateTimeFormatter.ISO_DATE));
        params.put("END_DATE", endDate.format(DateTimeFormatter.ISO_DATE));
        return params;
    }

    public JRDataSource getDataSource() {
        return new ReportDataSource(dates);
    }

    private void reduceDay(String date, BasicDBList sugars) {
        int count = sugars.size();
        int normCount = 0;
        int lessNormCount = 0;
        int aboveNormCount = 0;
        double min = Double.MAX_VALUE;
        double max = Double.MIN_VALUE;
        double avg = 0.0d;
        for (Object sugarObject : sugars) {
            DBObject sugar = (DBObject) sugarObject;
            double value = ((Number) sugar.get(SugarDAO.Fields.VALUE)).doubleValue();
            if (min > value) {
                min = value;
            }
            if (max < value) {
                max = value;
            }
            if (normMin > value) {
                lessNormCount++;
            }
            if (normMax < value) {
                aboveNormCount++;
            }
            if (normMin <= value && value < normMax) {
                normCount++;
            }
            avg += value;
        }
        avg = avg / count;
        DBObject day = new BasicDBObject();
        day.put(ReportFields.DATE, date);
        day.put(ReportFields.MINIMUM, min);
        day.put(ReportFields.MAXIMUM, max);
        day.put(ReportFields.COUNT, count);
        day.put(ReportFields.AVERAGE, (double) Math.round(avg * 100.0) / 100.0);
        day.put(ReportFields.NORMAL_RECORD_COUNT, normCount);
        day.put(ReportFields.LESS_NORMA_RECORD_COUNT, lessNormCount);
        day.put(ReportFields.ABOVE_NORMA_RECORD_COUNT, aboveNormCount);
        dates.add(day);
        totalCount += count;
        totalNormCount += normCount;
        totalLessNormCount += lessNormCount;
        totalAboveNormCount += aboveNormCount;
        if (totalMin > min) {
            totalMin = min;
        }
        if (totalMax < max) {
            totalMax = max;
        }
        totalAvg += avg;
    }
}

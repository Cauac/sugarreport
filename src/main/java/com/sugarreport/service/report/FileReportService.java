package com.sugarreport.service.report;

import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.export.ooxml.JRDocxExporter;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import org.springframework.core.io.Resource;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class FileReportService {

    private final JasperReport sugarReport;

    public FileReportService(Resource sugarReportFile) throws IOException, JRException {
        sugarReport = (JasperReport) JRLoader.loadObject(sugarReportFile.getFile());
    }

    public static final String CONTENT_TYPE_PDF = "application/pdf; name=\"Diabet-Life.pdf\"";
    public static final String CONTENT_DESCRIPTION_PDF = "attachment; filename=\"Diabet-Life.pdf\"";
    public static final String CONTENT_TYPE_DOCX = "application/vnd.openxmlformats-officedocument.wordprocessingml.document; name=\"Diabet-Life.docx\"";
    public static final String CONTENT_DESCRIPTION_DOCX = "attachment; filename=\"Diabet-Life.docx\"";
    public static final String CONTENT_TYPE_XSL = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet; name=\"Diabet-Life.xlsx\"";
    public static final String CONTENT_DESCRIPTION_XSL = "attachment; filename=\"Diabet-Life.xlsx\"";
    public static final String CONTENT_DESCRIPTION = "Content-Disposition";

    public void generateSugarReportPDF(SugarReport report, HttpServletResponse response) throws Exception {
        response.addHeader(CONTENT_DESCRIPTION, CONTENT_DESCRIPTION_PDF);
        response.setContentType(CONTENT_TYPE_PDF);
        JasperPrint jasperPrint = JasperFillManager.fillReport(sugarReport, report.getParams(), report.getDataSource());
        byte[] bytes = JasperExportManager.exportReportToPdf(jasperPrint);
        ServletOutputStream servletStream = response.getOutputStream();
        servletStream.write(bytes);
        servletStream.flush();
        servletStream.close();
    }

    public void generateSugarReportDOCX(SugarReport report, HttpServletResponse response) throws Exception {
        response.addHeader(CONTENT_DESCRIPTION, CONTENT_DESCRIPTION_DOCX);
        response.setContentType(CONTENT_TYPE_DOCX);
        JasperPrint jasperPrint = JasperFillManager.fillReport(sugarReport, report.getParams(), report.getDataSource());
        JRDocxExporter exporter = new JRDocxExporter();
        exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
        exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(response.getOutputStream()));
        exporter.exportReport();
    }

    public void generateSugarReportXLSX(SugarReport report, HttpServletResponse response) throws Exception {
        response.addHeader(CONTENT_DESCRIPTION, CONTENT_DESCRIPTION_XSL);
        response.setContentType(CONTENT_TYPE_XSL);
        JasperPrint jasperPrint = JasperFillManager.fillReport(sugarReport, report.getParams(), report.getDataSource());
        JRXlsxExporter exporter = new JRXlsxExporter();
        exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
        exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(response.getOutputStream()));
        exporter.exportReport();
    }
}

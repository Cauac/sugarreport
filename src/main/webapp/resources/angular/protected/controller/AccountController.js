var AccountController = function ($scope, $http, UserService) {

    $scope.alerts = [];
    $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
    };

    $scope.user = UserService.getUserInfo();
    $scope.showChangePasswordButton = true;


    $scope.changePassword = function () {
        $scope.showChangePasswordButton = false;
        $scope.newPassword = '';
    };

    $scope.applyNewPassword = function () {
        $scope.showChangePasswordButton = true;
        $http.post('user/savePassword', $scope.newPassword).success(function () {
            $scope.alerts.push({type: 'success', msg: 'Ваш новый пароль успешно сохранен!'});
        });
    };

//    Settings
    $scope.viewState = true;
    var settingsBackUp;

    $scope.changeSettings = function () {
        $scope.viewState = false;
        settingsBackUp = angular.copy($scope.user.settings);
    };

    $scope.saveSettings = function () {
        $scope.viewState = true;
        $http.post('user/saveSettings', $scope.user.settings).success(function () {
            $scope.alerts.push({type: 'success', msg: 'Настройки успешно сохранены!'});
        });
    };

    $scope.backUpSettings = function () {
        $scope.viewState = true;
        $scope.user.settings = settingsBackUp;
    };
};
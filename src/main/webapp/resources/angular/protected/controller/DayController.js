var DayController = function ($scope, $modal, $http, $filter, DayDataService, DayChartService, DATE_FORMAT) {

    $scope.currentDate = new Date();
    $scope.datePickerOpened = false;
    $scope.datePickerOptions = {
        startingDay: 1,
        showWeeks: false
    };
    var getPreviousDate = function () {
        var previousDay = new Date($scope.currentDate.getTime());
        previousDay.setDate(previousDay.getDate() - 1);
        return previousDay;
    };

    var previousDay;

    $scope.openDatePicker = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.datePickerOpened = true;
    };

    var dataUpdatedSuccess = function () {
        $scope.records = DayDataService.getRecords();
        $scope.sugars = DayDataService.getSugars();
        DayChartService.drawChart();
    };

    var dataUpdatedError = function () {
        console.log('data update error');
    };

    $scope.dateChanged = function () {
        previousDay = getPreviousDate();
        DayDataService.updateDaysData($filter('date')($scope.currentDate, DATE_FORMAT), $filter('date')(previousDay, DATE_FORMAT)).then(dataUpdatedSuccess, dataUpdatedError);
    };
    $scope.dateChanged();

    $scope.previousDay = function () {
        $scope.currentDate.setDate($scope.currentDate.getDate() - 1);
        $scope.dateChanged();
    };

    $scope.nextDay = function () {
        $scope.currentDate.setDate($scope.currentDate.getDate() + 1);
        $scope.dateChanged();
    };


    var addRecord = function (record) {
        if (record.comment == '') {
            delete record.comment;
        }
        var isCurrentDay = record.date == $filter('date')($scope.currentDate, DATE_FORMAT);
        DayDataService.isExist(record).then(
            function (isExist) {
                if (isExist) {
                    var modalInstance = $modal.open({
                        templateUrl: '/pages/protected/dialog/confirmUpdate.html',
                        controller: ConfirmUpdateDialog
                    });
                    modalInstance.result.then(function () {
                        DayDataService.updateRecord(record, isCurrentDay);
                        if (record.type == 'sugar') {
                            DayChartService.drawChart();
                        }
                    });
                } else {
                    DayDataService.addRecord(record, isCurrentDay);
                    if (record.type == 'sugar') {
                        DayChartService.drawChart();
                    }
                }
            }
        );

    };

    $scope.addSugar = function () {

        var modalInstance = $modal.open({
            templateUrl: '/pages/protected/dialog/addSugar.html',
            controller: AddValueDialog,
            resolve: {
                record: function () {
                    return {date: new Date($scope.currentDate.getTime()), value: 5.0, comment: '', type: 'sugar', activity: 0, stress: 0};
                }
            }
        });

        modalInstance.result.then(function (result) {
            if (result.stress == 'нет') {
                delete result.stress;
            }
            if (result.activity == 'нет') {
                delete result.activity;
            }
            addRecord(result);
        });
    };

    $scope.addFood = function () {

        var modalInstance = $modal.open({
            templateUrl: '/pages/protected/dialog/addFood.html',
            controller: AddValueDialog,
            resolve: {
                record: function () {
                    return {date: new Date($scope.currentDate.getTime()), value: 5.0, comment: '', type: 'food'};
                }
            }
        });

        modalInstance.result.then(function (result) {
            addRecord(result);
        });
    };

    $scope.addInjection = function () {

        var modalInstance = $modal.open({
            templateUrl: '/pages/protected/dialog/addInjection.html',
            controller: AddValueDialog,
            resolve: {
                record: function () {
                    return {date: new Date($scope.currentDate.getTime()), value: 10, comment: '', type: 'injection'};
                }
            }
        });

        modalInstance.result.then(function (result) {
            addRecord(result);
        });
    };

    $scope.removeValue = function (record) {
        DayDataService.removeValue(record);
        DayChartService.drawChart();
    };
};

var AddValueDialog = function ($scope, $modalInstance, $filter, record, DATE_FORMAT, TIME_FORMAT) {

    $scope.record = record;
    $scope.record.date.setMinutes(0, 0, 0);
    $scope.activities = ['нет', 'мало', 'средне', 'много'];
    $scope.stress = ['нет', 'мало', 'средне', 'много'];

    $scope.ok = function () {
        $scope.record.time = $filter('date')($scope.record.date, TIME_FORMAT);
        $scope.record.date = $filter('date')($scope.record.date, DATE_FORMAT);
        $modalInstance.close($scope.record);
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel')
    };
};

var ConfirmUpdateDialog = function ($scope, $modalInstance) {

    $scope.ok = function () {
        $modalInstance.close();
    };
    $scope.cancel = function () {
        $modalInstance.dismiss('cancel')
    };
};
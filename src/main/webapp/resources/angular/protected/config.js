(function () {
    'use strict';
    var App = angular.module('SugarReportProtected');

    App.config(['$routeProvider', function ($routeProvider) {

        $routeProvider.when('/day', {
            templateUrl: 'pages/protected/day.html',
            controller: DayController
        });

        $routeProvider.when('/report', {
            templateUrl: 'pages/protected/report.html',
            controller: ReportController
        });

        $routeProvider.when('/account', {
            templateUrl: 'pages/protected/account.html',
            controller: AccountController
        });

        $routeProvider.when('/contacts', {
            templateUrl: 'pages/public/contacts.html',
            controller: HomeController
        });

        $routeProvider.otherwise({redirectTo: '/day'});

    }]);
    var HomeController = function ($scope) {

    };

    App.factory('errorInterceptor', ['$q', '$rootScope', function ($q, $rootScope) {
        return {
            request: function (config) {
                return config || $q.when(config);
            },
            requestError: function (request) {
                return $q.reject(request);
            },
            response: function (response) {
                return response || $q.when(response);
            },
            responseError: function (response) {
                $rootScope.$emit('needSignIn');
                return $q.reject(response);
            }
        };
    }]);

    App.config(['$httpProvider', function ($httpProvider) {
        $httpProvider.interceptors.push('errorInterceptor');
        $httpProvider.defaults.headers.common['requestFrom'] = 'AngularJS';
    }]);
})();
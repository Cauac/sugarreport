(function () {
    'use strict';
    angular.module('SugarReportProtected').service('UserService', function ($http) {

        var user = {};

        this.initUser = function () {
            $http.get('/user/getUserInfo').success(function (result) {
                user.username = result.email;
                user.settings = result.settings;
            });
        };

        this.getUserInfo = function () {
            return user;
        };

    });
})();

package com.sugarreport.dao;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.sugarreport.validation.UserSettingsValidator;
import org.apache.commons.lang.StringUtils;

public class UserDAO extends MongoDAO {

    public interface Fields {
        public static final String EMAIL = "email";
        public static final String PASSWORD = "password";
        public static final String SETTINGS = "settings";

        public interface SettingFields {
            public static final String COMPARE_DAYS = "compareDays";
            public static final String SUGAR_NORM_MIN = "sugarNormMin";
            public static final String SUGAR_NORM_MAX = "sugarNormMax";
        }
    }

    private static final DBObject GET_PASSWORD = new BasicDBObject(Fields.PASSWORD, 1);
    private static final DBObject GET_SETTINGS = new BasicDBObject(Fields.SETTINGS, 1);
    private static final DBObject GET_SETTINGS_WITHOUT_IDENTITY = new BasicDBObject(Fields.SETTINGS, 1);
    private static final DBObject DEFAULT_USER_SETTINGS = new BasicDBObject();

    static {
        GET_SETTINGS_WITHOUT_IDENTITY.put(IDENTITY, 0);
        DEFAULT_USER_SETTINGS.put(UserDAO.Fields.SettingFields.COMPARE_DAYS, true);
        DEFAULT_USER_SETTINGS.put(UserDAO.Fields.SettingFields.SUGAR_NORM_MIN, 4.0);
        DEFAULT_USER_SETTINGS.put(UserDAO.Fields.SettingFields.SUGAR_NORM_MAX, 9.0);
    }

    public void addUser(DBObject user) {
        String email = user.removeField(Fields.EMAIL).toString();
        user.put(IDENTITY, email);
        collection.insert(user);
    }

    public boolean exist(String email) {
        return collection.findOne(new BasicDBObject(IDENTITY, email), WITH_IDENTITY) != null;
    }

    public String getUserPassword(String email) {
        DBObject user = collection.findOne(new BasicDBObject(IDENTITY, email), GET_PASSWORD);
        return user != null ? user.get(Fields.PASSWORD).toString() : null;
    }

    public DBObject getUserSettings(String email) {
        DBObject user = collection.findOne(new BasicDBObject(IDENTITY, email), GET_SETTINGS);
        if (user != null) {
            if (!user.containsField(Fields.SETTINGS)) {
                return DEFAULT_USER_SETTINGS;
            }
            return (DBObject) user.get(Fields.SETTINGS);
        }
        return null;
    }

    public DBObject getUserInfo(String email) {
        DBObject user = collection.findOne(new BasicDBObject(IDENTITY, email), GET_SETTINGS_WITHOUT_IDENTITY);
        if (user != null) {
            if (!user.containsField(Fields.SETTINGS)) {
                user.put(Fields.SETTINGS, DEFAULT_USER_SETTINGS);
            }
            user.put(Fields.EMAIL, email);
        }
        return user;
    }

    public void savePassword(String email, String password) {
        if (StringUtils.isNotBlank(password)) {
            collection.update(new BasicDBObject(IDENTITY, email), new BasicDBObject("$set", new BasicDBObject(Fields.PASSWORD, password)), true, false);
        }
    }

    public void saveSettings(String email, DBObject settings) {
        if (UserSettingsValidator.isValid(settings)) {
            collection.update(new BasicDBObject(IDENTITY, email), new BasicDBObject("$set", new BasicDBObject(Fields.SETTINGS, settings)), true, false);
        }
    }
}

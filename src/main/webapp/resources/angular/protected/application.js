(function () {
    'use strict';

    var SugarReportProtected = {};

    var App = angular.module('SugarReportProtected', ['ngRoute', 'ngAnimate', 'ui.bootstrap.alert', 'ui.bootstrap.modal', 'ui.bootstrap.datepicker', 'ui.bootstrap.timepicker']);

    App.constant('DATE_FORMAT', 'yyyy-MM-dd');
    App.constant('TIME_FORMAT', 'HH:mm');

    App.run(function ($rootScope, $http, $window, UserService) {
        $rootScope.$on('needSignIn', function () {
            $window.location.href = '/#/signIn';
        });
        UserService.initUser();
    });

})();

package com.sugarreport.web;

import com.sugarreport.service.auth.SecurityService;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/")
public class HelloController {

    @RequestMapping(method = RequestMethod.GET)
    public String index() {
        return "/pages/public/index.html";
    }

    @RequestMapping(value = "404", method = RequestMethod.GET)
    public ModelAndView page404() {
        return new ModelAndView("redirect:/#/404");
    }

    @RequestMapping(value = "500")
    public ModelAndView page500() {
        return new ModelAndView("redirect:/#/500");
    }

    @RequestMapping(value = "isAuthenticated", method = RequestMethod.GET, produces = MediaType.TEXT_PLAIN_VALUE)
    public
    @ResponseBody
    String isAuthenticated() {
        return SecurityService.currentUserId();
    }
}

package com.sugarreport.web;

import com.mongodb.DBObject;
import com.mongodb.util.JSON;
import com.sugarreport.dao.UserDAO;
import com.sugarreport.service.auth.SecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;

@Controller
@RequestMapping("/user")
public class UserController {

    @Autowired
    UserDAO userDAO;

    @RequestMapping(value = "getUserInfo", method = RequestMethod.GET)
    public @ResponseBody DBObject getUserInfo(HttpServletResponse httpServletResponse) {
        httpServletResponse.setHeader("Content-Type", "application/json; charset=utf-8");
        return userDAO.getUserInfo(SecurityService.currentUserId());
    }

    @RequestMapping(value = "savePassword", method = RequestMethod.POST)
    public void savePassword(@RequestBody String newPassword, HttpServletResponse response) {
        userDAO.savePassword(SecurityService.currentUserId(), newPassword);
        response.setStatus(HttpServletResponse.SC_OK);
    }

    @RequestMapping(value = "saveSettings", method = RequestMethod.POST)
    public void saveSettings(@RequestBody String settingJson, HttpServletResponse response) {
        userDAO.saveSettings(SecurityService.currentUserId(), (DBObject) JSON.parse(settingJson));
        response.setStatus(HttpServletResponse.SC_OK);
    }
}

package com.sugarreport.web;

import ch.qos.logback.classic.Logger;
import com.mongodb.DBObject;
import com.mongodb.util.JSON;
import com.sugarreport.dao.UserDAO;
import com.sugarreport.service.mail.MailService;
import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.LoggerFactory;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;

import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;
import java.util.LinkedHashMap;
import java.util.Map;

@Controller
@RequestMapping("/register")
public class RegisterController {

    private static Logger errorLogger = (Logger) LoggerFactory.getLogger("ERROR-LOGGER");
    private static Logger notificationLogger = (Logger) LoggerFactory.getLogger("NOTIFICATION-LOGGER");
    private static Marker marker = MarkerFactory.getMarker("SEND_MAIL");

    @Autowired
    private MailService mailService;

    @Autowired
    private UserDAO userDAO;

    @Value("#{mail.confirmation_message_subject}")
    private String confirmationMessageSubject;

    @Value("#{mail.confirmation_message_text}")
    private String confirmationMessageText;

    @Value("#{mail.confirmation_queue_size}")
    private int confirmationQueueSize;


    @Value("#{web.application_url}")
    private String APPLICATION_URL;

    private LinkedHashMap<Long, DBObject> confirmationMap;

    public RegisterController() {
        confirmationMap = new LinkedHashMap<Long, DBObject>(confirmationQueueSize) {
            @Override
            protected boolean removeEldestEntry(Map.Entry eldest) {
                return size() > confirmationQueueSize;
            }
        };
    }

    @RequestMapping(method = RequestMethod.POST)
    public
    @ResponseBody
    void register(@RequestBody String jsonData, HttpServletResponse httpServletResponse) {
        try {
            DBObject user = (DBObject) JSON.parse(jsonData);
            String email = StringUtils.trim(ObjectUtils.toString(user.get(UserDAO.Fields.EMAIL)));
            String password = StringUtils.trim(ObjectUtils.toString(user.get(UserDAO.Fields.PASSWORD)));

            if (StringUtils.isEmpty(email) || StringUtils.isEmpty(password)) {
                httpServletResponse.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                return;
            }
            if (userDAO.exist(email)) {
                httpServletResponse.setStatus(HttpServletResponse.SC_CONFLICT);
                return;
            }

            user.put(UserDAO.Fields.EMAIL, email);
            user.put(UserDAO.Fields.PASSWORD, password);
            long key = System.currentTimeMillis();
            confirmationMap.put(key, user);

            mailService.sendRegisterMessage(email, confirmationMessageSubject, String.format(confirmationMessageText, "\r\n" + APPLICATION_URL + "register/confirmation/" + key));
            httpServletResponse.setStatus(HttpServletResponse.SC_CREATED);

        } catch (Exception ex) {
            httpServletResponse.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            errorLogger.error("Registration error jsonData:" + jsonData, ex);
        }

    }

    @RequestMapping(value = "confirmation/" + "{code}", method = RequestMethod.GET)
    public ModelAndView confirmation(@PathVariable("code") Long code) {
        try {
            DBObject user = confirmationMap.remove(code);
            if (user == null) {
                return new ModelAndView("redirect:" + APPLICATION_URL + "#/signUp?lost");
            }
            String email = ObjectUtils.toString(user.get(UserDAO.Fields.EMAIL));
            userDAO.addUser(user);
            notificationLogger.info(marker, "New user : {}", email);
            return new ModelAndView("redirect:" + APPLICATION_URL + "#/signIn?successRegister");
        } catch (Exception ex) {
            errorLogger.error("Registration error code:" + code, ex);
            return new ModelAndView("redirect:" + APPLICATION_URL + "#/500");
        }
    }
}

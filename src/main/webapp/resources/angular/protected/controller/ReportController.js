var ReportController = function ($scope, $http, UserService, $filter, DATE_FORMAT) {

    $scope.startDate = new Date();
    $scope.endDate = new Date();
    $scope.startDatePickerOpened = false;
    $scope.endDatePickerOpened = false;
    $scope.datePickerOptions = {
        startingDay: 1,
        showWeeks: false
    };

    $scope.startDateChanged = function () {
        $scope.startDateFormatted = $filter('date')($scope.startDate, DATE_FORMAT);
    };

    $scope.endDateChanged = function () {
        $scope.endDateFormatted = $filter('date')($scope.endDate, DATE_FORMAT);
    };

    $scope.openStartDatePicker = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.startDatePickerOpened = true;
    };
    $scope.openEndDateDatePicker = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.endDatePickerOpened = true;
    };

    var initWeek = function () {
        var date = new Date();
        var currentDay = date.getDay();
        var distance = (currentDay == 0 ? 6 : currentDay - 1);
        $scope.startDate.setDate(date.getDate() - distance);
        $scope.endDate.setDate(date.getDate() - distance + 6);
        $scope.startDateChanged();
        $scope.endDateChanged();
    };
    initWeek();

    $scope.report = function () {
        var start = new Date($scope.startDate.getTime());
        var dates = $filter('date')(start, DATE_FORMAT);
        while (start < $scope.endDate) {
            start.setDate(start.getDate() + 1);
            dates = dates + ';' + $filter('date')(start, DATE_FORMAT);
        }
        $http.post('/report/getData', dates).success(function (result) {
            $scope.reportData = result;
            if (result && result.dates.length != 0) {
                $scope.pieData = [{"type": "В пределах нормы", "value": $scope.reportData.normCount},{"type": "Ниже нормы", "value": $scope.reportData.lessNormCount},{"type": "Выше нормы", "value": $scope.reportData.aboveNormCount}];
            } else {
                $scope.pieData = [];
            }
            drawPie();

        });
    };

    var drawPie = function () {
        AmCharts.makeChart("pie", {
            "type": "pie",
            "theme": "light",
            "dataProvider": $scope.pieData,
            "valueField": "value",
            "titleField": "type"
        });
    };
};

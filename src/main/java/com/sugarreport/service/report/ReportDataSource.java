package com.sugarreport.service.report;

import com.mongodb.BasicDBList;
import com.mongodb.DBObject;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

import java.util.Iterator;

public class ReportDataSource implements JRDataSource {

    private Iterator<Object> iterator;
    private DBObject current;

    public ReportDataSource(BasicDBList days) {
        iterator = days.iterator();
    }

    @Override
    public boolean next() throws JRException {
        if (iterator.hasNext()) {
            current = (DBObject) iterator.next();
            return true;
        }
        return false;
    }

    @Override
    public Object getFieldValue(JRField jrField) throws JRException {
        return current.get(jrField.getName());
    }
}
